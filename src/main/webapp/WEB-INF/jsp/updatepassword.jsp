<%--
  Created by IntelliJ IDEA.
  User: elchi
  Date: 18.01.2020
  Time: 15:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>Update Password</title>
</head>
<body>
<c:if test="${not empty requestScope.PasswordUnEqual}">
 <span class="error">${requestScope.PasswordUnEqual}</span>
</c:if>

<form method="post" action="/updatePassword">
    New Password : <input type="password" placeholder="New Password" name="password"/><br/>
    Confirm Your Password : <input type="password" placeholder="Confirmation Password" name="confirmpass"/><br/>
    <input type="Submit" value="Sumbit"/>
</form>
</body>
</html>
