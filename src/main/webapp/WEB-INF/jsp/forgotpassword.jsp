<%--
  Created by IntelliJ IDEA.
  User: elchi
  Date: 17.01.2020
  Time: 19:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Forgot Password</title>
    <style>
        .error{
            color: red;
        }
    </style>
</head>
<body>

<%--<form method="post" action="forgot">--%>
<%--    Enter Your Email : <input type="text" name="email"/>--%>
<%--    <input type="submit" value="Submit"/>--%>
<%--</form>--%>
<form:form modelAttribute="email" method="post" action="forgot">
        Enter Your Email : <form:input path="email" />
        <form:errors path="email" cssClass="error"/>
        <input type="submit" value="Submit"/>
</form:form>
</body>
</html>
