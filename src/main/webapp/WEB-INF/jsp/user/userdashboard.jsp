<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Welcome User Dashboard</title>
</head>
<body>

Welcome , ${user.name} ${user.surname } ! <br/>
Your ${requestScope.imageCount} image available in us !
<c:if test="${requestScope.imageCount > 0}">
    If you want to see your pictures , <a href="/user/image">click here</a>
</c:if>


<br/><br/><br/>
You can add more images to us :)
<form method="post" action="/user/insert/image" enctype="multipart/form-data">
    <span style="color: red"> Your images size must be max 10MB ! </span><br/>
    Choose your files : <input type="file" multiple="multiple" name="images"/><br/>
    <input type="submit"/>
</form><br/><br/>


<a href="/logout">Logout</a>
</body>
</html>
