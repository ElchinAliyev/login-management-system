<%--
  Created by IntelliJ IDEA.
  User: nadir
  Date: 5/17/2020
  Time: 2:04 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Images</title>
</head>
<body>
<span style="color: red; font-size: 30px"> Your images </span> : <br/>
<c:forEach items="${requestScope.imageList}" var="image">
    <span>
    <img src="/user/${image.id}/image" width="300px" height="250px">
        <button> <a href="/user/download/image/${image.id}" style="text-decoration: none">Download</a></button>
    </span>

</c:forEach>

<br/><br/>
<span style="font-size: 20px"><a href="/user/" >Click here </a> to go dashboard ! </span>
</body>
</html>
