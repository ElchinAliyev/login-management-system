
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up</title>


    <!-- Font Icon -->
    <link rel="stylesheet" href="/register/fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="/register/css/style.css">
</head>
<body>

<div class="main">

    <section class="signup">
        <!-- <img src="images/signup-bg.jpg" alt=""> -->
        <div class="container">
            <div class="signup-content">
                <form:form method="POST" modelAttribute="user" action="register"  class="signup-form">
                    <h2 class="form-title">Create account</h2>
                    <div class="form-group">
                        <form:input path="name" cssClass="form-input" placeholder="Your Name"/>
                        <form:errors path="name" cssClass="error"/>
                    </div>
                    <div class="form-group">
                        <form:input path="surname" cssClass="form-input" placeholder="Your Surname"/><br/>
                        <form:errors path="surname" cssClass="error"/>
                    </div>
                    <div class="form-group">
                        <form:input path="email" cssClass="form-input" placeholder="Your Email"/><br/>
                        <form:errors path="email" cssClass="error"/>
                    </div>
                    <div class="form-group">
                        <form:password path="password" cssClass="form-input" placeholder="Your Password"/><br/>
                        <form:errors path="password" cssClass="error"/>
                        <span toggle="#password" class="zmdi zmdi-eye field-icon toggle-password"></span>
                    </div>
                    <div class="form-group">
                        <form:password path="confirmationPassword" cssClass="form-input" placeholder="Password Confirmation"/><br/>
                        <form:errors path="*" cssClass="error"/>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" />
                        <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all statements in  <a href="#" class="term-service">Terms of service</a></label>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit" id="submit" class="form-submit" value="Sign up"/>
                    </div>
                </form:form>
                <p class="loginhere">
                    Have already an account ? <a href="/" class="loginhere-link">Login here</a>
                </p>
            </div>
        </div>
    </section>

</div>

<!-- JS -->
<script src="/register/vendor/jquery/jquery.min.js"></script>
<script src="/register/vendor/register/js/main.js"></script>
</body>
</html>