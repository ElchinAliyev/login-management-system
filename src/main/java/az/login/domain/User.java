package az.login.domain;

import az.login.validatorannotation.FieldEquals;
import az.login.validatorannotation.NotDuplicateEmail;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@FieldEquals
@Entity
@Table(name = "user_info")
public class User implements Serializable {

    private static final long serialVersionUID = 1990994373914104382L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id ;

    @NotBlank(message = "{user.name.blank}")
    @Size(min = 2 , max = 20 ,message = "{user.name.length}")
    @Column(length = 300)
    private String name;
    @NotBlank(message = "{user.surname.blank}")
    @Size(min = 2 , max = 20 , message = "{user.surname.length}")
    private String surname;

    @OneToMany(mappedBy = "user")
    private List<Token> token;

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    @OneToMany(mappedBy = "user")
    private List<Image> images;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", status=" + status +
                ", confirmationPassword='" + confirmationPassword + '\'' +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Email(message = "{user.email}")
    @NotDuplicateEmail
    private String email;
    @Size(min = 8 , max = 200 , message = "{user.password.length}")
    private String password;
    private int status;

    private String confirmationPassword;


    public User() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmationPassword() {
        return confirmationPassword;
    }

    public void setConfirmationPassword(String confirmationPassword) {
        this.confirmationPassword = confirmationPassword;
    }


    public List<Token> getToken() {
        return token;
    }

    public void setToken(List<Token> token) {
        this.token = token;
    }







}
