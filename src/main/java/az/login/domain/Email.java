package az.login.domain;

import javax.sql.rowset.serial.SerialArray;
import java.io.Serializable;

public class Email implements Serializable {
    private String email;

    public Email() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
