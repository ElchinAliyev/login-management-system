package az.login.util;

public enum UserStatus {


    PENDING(1),
    ACTIVE(2),
    BLOCKED(3),
    DELETED(4);

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    UserStatus(int value) {
        this.value = value;
    }

    private int value;

    public static UserStatus getUserStatus(int val){
        if (val==1){
            return UserStatus.PENDING;
        }
        else if(val==2){
            return UserStatus.ACTIVE;
        }
        else if(val==3){
            return UserStatus.BLOCKED;
        }
        else if(val==4){
            return UserStatus.DELETED;
        }
        else {
            throw new RuntimeException("Bele User Status Tapilmadi");
        }

    }



}
