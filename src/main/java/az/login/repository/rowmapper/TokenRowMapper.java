package az.login.repository.rowmapper;

import az.login.domain.Token;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class TokenRowMapper implements RowMapper<Token> {

    /*
     *
     * select t.id , t.token , t.user_id , u.email , u.name , u.surname  from token t
join user_info u on u.id = t.user_id and t.status =2 ;

     *
     * */
    @Override
    public Token mapRow(ResultSet resultSet, int i) throws SQLException {
        Token token = new Token();
        token.setId(resultSet.getLong("id"));
        token.getUser().setId(resultSet.getLong("userid"));
        token.getUser().setEmail(resultSet.getString("email"));
        token.getUser().setName(resultSet.getString("name"));
        token.getUser().setSurname(resultSet.getString("surname"));
        token.setToken(resultSet.getString("token"));


        return token;
    }
}
