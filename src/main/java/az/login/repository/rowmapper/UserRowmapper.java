package az.login.repository.rowmapper;

import az.login.domain.User;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UserRowmapper implements RowMapper<User> {

    // public static String existEmail = " select id , name , surname , email , password from user_info where email = :email ";

    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();

        user.setId(resultSet.getLong("id"));
        user.setName(resultSet.getString("name"));
        user.setSurname(resultSet.getString("surname"));
        user.setEmail(resultSet.getString("email"));
        user.setPassword(resultSet.getString("password"));
  //      user.setStatus(resultSet.getInt("status"));
        return user;
    }
}
