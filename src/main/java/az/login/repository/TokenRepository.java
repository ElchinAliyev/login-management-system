package az.login.repository;

import az.login.domain.Token;
import az.login.domain.User;

import java.util.List;
import java.util.Optional;

public interface TokenRepository {
    Token insertToken(Token token);
     List<Token> getUnusedTokens();
     void updateTokenStatus(long tokenId);
     Optional<User> getUserByToken(String token);
     Token insertTokenForUpdatePassword(Token token);
     List<Token> getUnusedTokenForUpdatePassword();
     void updateStatusTokenForUpdatePass(long tokenId);
     Optional<User> getUserByTokenForUpdatePass(String token);
}
