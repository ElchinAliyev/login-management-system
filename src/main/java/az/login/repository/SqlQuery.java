package az.login.repository;

public class SqlQuery {
    public static String existEmail = " select id , name , surname , email , password from user_info where email = :email ";
    public static String insertUser = "insert into user_info(name , surname , email,password,status)" +
            " VALUES (:name , :surname,:email , :password , 1  ) " ;
    public static String insertToken = " insert into token(userid,token,status) VALUES(:userId,:token,2) ";
    public static String getUnusedTokens = "select t.id , t.token , t.userid , u.email , u.name , u.surname  from token t\n" +
            "join user_info u on u.id = t.userid and t.status =2 ";
    public static String updateTokenStatus = " update token set status=1 where id = :tokenId ";
    public static String getUserByToken = " select u.id , u.email , u.NAME,u.SURNAME,u.password,u.status from user_info u " +
            " join token t on t.userid = u.id  where t.token = :token " ;
    public static String updateUserStatus = " update user_info set  status = :status  where id = :id; ";
    public static String getUserByEmail = " select id , email , password , status , name , surname from user_info " +
            " where email = :email ";
    public static String insertTokenForUpdatePassword="insert into tokenupdatepass(userid,token,status) VALUES(:userId,:token,2) ";
    public static String getUnusedTokensForUpdatePass = "select t.id , t.token , t.userid , u.email , u.name , u.surname " +
            "  from tokenupdatepass t " +
            " join user_info u on u.id = t.userid and t.status =2 ";
    public static String updateStatusTokenForUpdatePass =" update tokenupdatepass set status=1 where id = :tokenId";
    public static String getUserByTokenForUpdatePassword = " select u.id , u.email , u.NAME,u.SURNAME,u.password,u.status from user_info u " +
           " join tokenupdatepass t on t.userid = u.id  where t.token = :token ";
    public static String updateUserPasswordByUserId = " update user_info set password = :password where id =:userId ";
}
