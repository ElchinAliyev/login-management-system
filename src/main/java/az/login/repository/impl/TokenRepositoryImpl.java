package az.login.repository.impl;

import az.login.domain.Token;
import az.login.domain.User;
import az.login.repository.SqlQuery;
import az.login.repository.TokenRepository;
import az.login.repository.rowmapper.TokenRowMapper;
import az.login.repository.rowmapper.UserRowmapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public class TokenRepositoryImpl implements TokenRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    private TokenRowMapper tokenRowMapper;
    @Autowired
    private UserRowmapper userRowmapper;

    /*
     *  public static String insertToken = " insert into token(user_id,token,2) VALUES(:userId,:token) ";
     * */

    @Override
    public Token insertToken(Token token) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource()
                .addValue("userId", token.getUser().getId())
                .addValue("token", token.getToken());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        int count = jdbcTemplate.update(SqlQuery.insertToken, mapSqlParameterSource, keyHolder);
        if (count > 0) {
            token.setId((Long) keyHolder.getKeys().get("id"));
            return token;
        } else {
            throw new RuntimeException("Token add edilemedi " + token);
        }

    }

    @Override
    public List<Token> getUnusedTokens() {

        List<Token> tokenList = jdbcTemplate.query(SqlQuery.getUnusedTokens, new MapSqlParameterSource(), tokenRowMapper);
        System.out.println("Unused " + tokenList);

        return tokenList;
    }

    /*
     *  public static String updateTokenStatus = " update token set status=1 where id = :tokenId ";
     * */
    @Override
    public void updateTokenStatus(long tokenId) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("tokenId", tokenId);
        int count = jdbcTemplate.update(SqlQuery.updateTokenStatus, mapSqlParameterSource);
        if (count == 0) {
            throw new RuntimeException("Token Update edilenmedi " + tokenId);
        }
    }

    /*
    * select u.id , u.email , u.NAME,u.SURNAME,u.password,u.status from user_info u
join token t on t.user_id = u.id  where t.token = :token;
    * */

    @Override
    public Optional<User> getUserByToken(String token) {
        Optional<User> optionalUser = Optional.empty();
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource("token", token);
        List<User> users = jdbcTemplate.query(SqlQuery.getUserByToken, mapSqlParameterSource, userRowmapper);

        if (!users.isEmpty()) {
            optionalUser = Optional.of(users.get(0));
        }

        return optionalUser;
    }

    /*
     *   public static String insertTokenForUpdatePassword=
     * "insert into tokenupdatepass(user_id,token,status) VALUES(:userId,:token,2) ";
     *
     * */
    @Override
    public Token insertTokenForUpdatePassword(Token token) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource()
                .addValue("userId", token.getUser().getId())
                .addValue("token", token.getToken());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        int count = jdbcTemplate.update(SqlQuery.insertTokenForUpdatePassword, mapSqlParameterSource, keyHolder);

        if (count > 0) {
            token.setId((Integer) keyHolder.getKeys().get("id"));
            return token;
        } else {
            throw new RuntimeException("Token add edilenmedi " + token);
        }
    }

    /*    public static String getUnusedTokensForUpdatePass = "select t.id , t.token , t.user_id , u.email , u.name , u.surname " +
                "  from tokenupdatepass t " +
                " join user_info u on u.id = t.user_id and t.status =2 ";
    * */
    @Override
    public List<Token> getUnusedTokenForUpdatePassword() {
        return jdbcTemplate.query(SqlQuery.getUnusedTokensForUpdatePass, new MapSqlParameterSource(), tokenRowMapper);

    }
/*
*
* public static String updateStatusTokenForUpdatePass =" update tokenupdatepass set status=1 where id = :tokenId";
*
* */
    @Override
    public void updateStatusTokenForUpdatePass(long tokenId) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("tokenId",tokenId);
       int count =  jdbcTemplate.update(SqlQuery.updateStatusTokenForUpdatePass,mapSqlParameterSource);
       if (count==0){
           throw new RuntimeException("Can not update token status" + tokenId);
       }
    }
/*
* public static String getUserByTokenForUpdatePassword = " select u.id , u.email , u.NAME,u.SURNAME,u.password,u.status from user_info u \" +\n" +
            "            \" join tokenupdatepass t on t.user_id = u.id  where t.token = :token ";
*
* */
    @Override
    public Optional<User> getUserByTokenForUpdatePass(String token) {
        Optional<User> optionalUser = Optional.empty();
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("token",token);

       List<User> users =  jdbcTemplate.query(SqlQuery.getUserByTokenForUpdatePassword,mapSqlParameterSource,userRowmapper);
       if (!users.isEmpty()){
       optionalUser = Optional.of(users.get(0));
       }

        return optionalUser;
    }
}
