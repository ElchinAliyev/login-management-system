package az.login.repository.impl;

import az.login.domain.User;
import az.login.repository.SqlQuery;
import az.login.repository.UserRepository;
import az.login.repository.rowmapper.UserRowmapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {
    @Autowired
    private UserRowmapper userRowmapper;
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

/*
*     public static String insertUser = "insert into user_info(name , surname , email,password,status)" +
            " VALUES (:name , :surname,:email , :password , 1  ) " ;
* */

    @Override
    public User insertUser(User user) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource("name", user.getName())
                .addValue("surname", user.getSurname())
                .addValue("email", user.getEmail())
                .addValue("password", user.getPassword());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        int count = jdbcTemplate.update(SqlQuery.insertUser, mapSqlParameterSource, keyHolder);
        if (count > 0) {
            user.setId((Long) keyHolder.getKeys().get("id"));
            return user;
        } else {
            throw new RuntimeException("User insert edile bilmedi " + user);
        }

    }

    /*
     * public static String updateUserStatus = " update user_info set  status = :status  where id = :id; ";
     *
     * */
    @Override
    public void updateUserStatus(long userId, long statusId) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource()
                .addValue("status", statusId)
                .addValue("id", userId);

        int count = jdbcTemplate.update(SqlQuery.updateUserStatus, mapSqlParameterSource);
        if (count == 0) {
            throw new RuntimeException("Userin statusu deyislemmedi" + userId);
        }
    }

    /*
    * select id , email , password , status , name , surname from user_info
    where email = :email;
    *
    * */
    @Override
    public Optional<User> getUser(String email) {
        Optional<User> optionalUser = Optional.empty();
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource("email", email);

        List<User> users = jdbcTemplate.query(SqlQuery.getUserByEmail, mapSqlParameterSource, userRowmapper);
        if (!users.isEmpty()) {
            optionalUser = Optional.of(users.get(0));
        }


        return optionalUser;
    }


    /*
     * public static String updateUserPasswordByUserId = " update user_info set password = :password where id =:userId ";
     * */
    @Override
    public void updateUserPass(long userId, String password) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource()
                .addValue("password", password)
                .addValue("userId", userId);

        int count = jdbcTemplate.update(SqlQuery.updateUserPasswordByUserId, mapSqlParameterSource);
        if (count == 0) {
            throw new RuntimeException("Can not update user password " + userId);
        }
    }
}
