package az.login.repository.impl;

import az.login.domain.User;
import az.login.repository.EmailRepository;
import az.login.repository.SqlQuery;
import az.login.repository.rowmapper.UserRowmapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EmailRepositoryImpl implements EmailRepository {


    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private UserRowmapper userRowmapper;


//   public static String existEmail = " select id , name , surname , email , password from user_info where email = :email ";
    @Override
    public boolean emailExists(String email) {

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource("email" ,email);

     List<User> userList =  jdbcTemplate.query(SqlQuery.existEmail,sqlParameterSource,userRowmapper);
     if (!userList.isEmpty()){

         return true;
     }
     return false;
    }
}
