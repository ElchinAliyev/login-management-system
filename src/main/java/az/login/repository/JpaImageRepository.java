package az.login.repository;

import az.login.domain.Image;
import az.login.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JpaImageRepository extends JpaRepository<Image,Integer> {


    List<Image> getAllByUser(User user);
}
