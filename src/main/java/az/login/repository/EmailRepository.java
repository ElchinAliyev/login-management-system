package az.login.repository;

public interface EmailRepository {
    public boolean emailExists(String email);
}
