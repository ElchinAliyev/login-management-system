package az.login.repository;

import az.login.domain.User;

import java.util.Optional;

public interface UserRepository {

     User insertUser(User user);
     void updateUserStatus(long userId , long statusId);
     Optional<User> getUser(String email);
     void updateUserPass(long userId,String password);
}
