package az.login.controller;

import az.login.domain.Email;
import az.login.domain.Token;
import az.login.domain.User;
import az.login.service.TokenService;
import az.login.service.UserService;
import az.login.util.UserStatus;
import az.login.validator.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class WebController {

    @Autowired
    private EmailValidator emailValidator;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @GetMapping("/")
    public String root() {
        return "login";
    }

    @InitBinder
    public void dataBinder(WebDataBinder dataBinder) {
        if (dataBinder.getTarget() != null && dataBinder.getTarget().getClass().equals(Email.class)) {
            dataBinder.setValidator(emailValidator);
        }

    }

//    @PostMapping("/")
//    public ModelAndView login(@RequestParam(name = "email") String email,
//                              @RequestParam(name = "password") String password) {
//        System.out.println(email);
//        System.out.println(password);
//        System.out.println("Controllerdeki  /  Post isledi !");
//        ModelAndView modelAndView = new ModelAndView("login");
//        return modelAndView;
//    }

    @GetMapping("/register")
    public ModelAndView register() {
        ModelAndView modelAndView = new ModelAndView("register");
        User user = new User();
        modelAndView.addObject(user);

        return modelAndView;
    }

    @PostMapping("/register")
    public ModelAndView modelAndView(@ModelAttribute(name = "user") @Valid User user, BindingResult result, HttpSession session, RedirectAttributes redirectAttributes) {
        System.out.println(user);

        ModelAndView modelAndView = new ModelAndView();

        if (result.hasErrors()) {
            modelAndView.setViewName("register");
        } else {

            User user1 = userService.insertUser(user);
            session.setAttribute("user", user1);
            modelAndView.setViewName("redirect:/");
        }


        return modelAndView;
    }

    @GetMapping("/activate")
    public ModelAndView activateProfile(@RequestParam(name = "token") String token, HttpSession session,RedirectAttributes redirectAttributes) {
        ModelAndView modelAndView = new ModelAndView();

        Optional<User> optionalUser = tokenService.getUserByToken(token);
        session.removeAttribute("user");
        if (optionalUser.isPresent()) {

            User user = optionalUser.get();

            System.out.println(user);
            user.setStatus(1);
            UserStatus userStatus = UserStatus.getUserStatus(user.getStatus());
            if (userStatus == UserStatus.PENDING) {
                userService.updateUserStatus(user.getId(), UserStatus.ACTIVE.getValue());
                redirectAttributes.addFlashAttribute("congurulation", "Dear user you sign up successfully!");
                modelAndView.setViewName("redirect:/");
            } else {
                modelAndView.setViewName("redirect:/error.jsp");
            }

        } else {
            modelAndView.setViewName("redirect:/error.jsp");
        }


        return modelAndView;
    }

    @GetMapping("/forgot")
    public ModelAndView forgotPassword() {
        ModelAndView modelAndView = new ModelAndView("forgotpassword");
        Email email = new Email();
        modelAndView.addObject(email);
        return modelAndView;
    }

    @PostMapping("/forgot")
    public ModelAndView forgotPassword(@ModelAttribute(name = "email") @Valid Email email, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();


        if (!bindingResult.hasErrors()) {
            String mail = email.getEmail();
            Optional<User> optionalUser = userService.getUser(mail);
            if (optionalUser.isPresent()) {
                User user = optionalUser.get();
                Token token = tokenService.insertTokenForUpdatePassword(user);
                modelAndView.addObject("Message", "Check Your Email Please");
                modelAndView.setViewName("login");
            } else {
                modelAndView.addObject("NotFound", "Bu maile uygun istifadeci yoxdur");
                modelAndView.setViewName("login");
            }
        } else {
            System.out.println("Mail error var");
            //bindingResult.getAllErrors().forEach(x-> System.out.println(x));
            System.out.println(bindingResult.getModel());
            modelAndView.setViewName("forgotpassword");
        }
        return modelAndView;
    }

      @GetMapping("/updatePassword")
    public ModelAndView modelAndView(@RequestParam(name = "token") String token,HttpSession session){
        ModelAndView modelAndView = new ModelAndView();
            Optional<User> optionalUser = tokenService.getUserByTokenForUpdatePass(token);
            if (optionalUser.isPresent()){
                User user = optionalUser.get();
                session.setAttribute("user",user);
                modelAndView.setViewName("updatepassword");
            }
            else {
                modelAndView.setViewName("redirect:/error.jsp");
            }

        return modelAndView;
      }


      @PostMapping("/updatePassword")
    public ModelAndView updatePassword(
            @RequestParam(name = "password") String password,
            @RequestParam(name = "confirmpass") String confirmPass,
            HttpSession session
      ){

          ModelAndView modelAndView = new ModelAndView();
        if (password.equals(confirmPass)){
            User user = (User) session.getAttribute("user");
            userService.updateUserPass(user.getId(),password);
            session.setAttribute("passwordSuccess","Your Password Update Successfully");
            modelAndView.setViewName("redirect:/");
        }
        else {
            modelAndView.addObject("PasswordUnEqual","Password Doesn't Match Each Other");
            modelAndView.setViewName("updatepassword");
        }




        return modelAndView;
      }
}
