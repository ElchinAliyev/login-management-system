package az.login.controller;


import az.login.domain.Image;
import az.login.domain.User;
import az.login.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Controller
@RequestMapping("/user/")
public class UserController {

    @Autowired
    private ImageService imageService;


    @GetMapping("/")
    public ModelAndView root(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("user");
        ModelAndView modelAndView = new ModelAndView("user/userdashboard");
        int imageCount = imageService.getUserImageCount(user);

        modelAndView.addObject("imageCount", imageCount);

        return modelAndView;
    }

    @PostMapping("/insert/image")
    public String insertImage(HttpServletRequest request, MultipartFile[] images) {
        User user = (User) request.getSession().getAttribute("user");
        List<Image> imageList = imageService.insertImage(user.getId(), images);
        return "redirect:/user/";
    }

    @GetMapping("/image")
    public ModelAndView getImages(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("/user/image");
        User user = (User) request.getSession().getAttribute("user");
        List<Image> imageList = imageService.getImages(user);
        modelAndView.addObject("imageList", imageList);
        return modelAndView;
    }

    @ResponseBody
    @GetMapping("/{id}/image")
    public Resource getImageById(@PathVariable(value = "id") int id, HttpServletResponse response) {
        Resource resource = imageService.getImageById(id);

        try {
            String fileType = Files.probeContentType(Paths.get(resource.getFile().getPath()));
            response.setContentType(fileType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resource;
    }

    @GetMapping("/download/image/{id}")
    @ResponseBody
    public Resource downloadImage(@PathVariable(value = "id") int id, HttpServletResponse response) {
        Resource resource = imageService.getImageById(id);

        try {
            String contentType = Files.probeContentType(Paths.get(resource.getFile().getPath()));
            response.setContentType(contentType);
            response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + resource.getFilename());
            response.setContentLength((int) resource.getFile().length());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return resource;
    }

}
