package az.login.validatorannotation;


import az.login.validatorannotation.constraints.DuplicateEmailValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Size;
import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(
        validatedBy = {DuplicateEmailValidator.class}
)
public @interface NotDuplicateEmail{

    String message() default "{email.duplicate}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
