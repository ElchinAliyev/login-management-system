package az.login.validatorannotation;

import az.login.validatorannotation.constraints.DuplicateEmailValidator;
import az.login.validatorannotation.constraints.PasswordEqualityValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(
        validatedBy = {PasswordEqualityValidator.class}
)
public @interface FieldEquals {
    String message() default "{user.password.equality}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
