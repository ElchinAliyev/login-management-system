package az.login.validatorannotation.constraints;

import az.login.domain.User;
import az.login.validatorannotation.FieldEquals;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordEqualityValidator implements ConstraintValidator<FieldEquals,Object> {


    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        User user = (User) o;
       if (user.getPassword()!=null && user.getConfirmationPassword()!=null
               &&user.getPassword().equals(user.getConfirmationPassword())){
           return true;
       }
        return false;
    }
}
