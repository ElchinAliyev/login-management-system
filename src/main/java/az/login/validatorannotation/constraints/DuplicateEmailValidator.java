package az.login.validatorannotation.constraints;

import az.login.service.EmailService;
import az.login.validatorannotation.NotDuplicateEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class DuplicateEmailValidator implements ConstraintValidator<NotDuplicateEmail,String> {
    @Autowired
    private EmailService emailService;
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
         return !emailService.emailExists(s);

    }
}
