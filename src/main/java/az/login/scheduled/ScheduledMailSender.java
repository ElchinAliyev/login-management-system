package az.login.scheduled;


import az.login.domain.Token;
import az.login.service.EmailService;
import az.login.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component

public class ScheduledMailSender {


    @Autowired
    private TokenService tokenService;

    @Autowired
    private EmailService emailService;


    @Value("${activation.base.url}")
    private String activationBaseUrl;
    @Value("${activation.sender.email}")
    private String emailSender;
    @Value("${base.url}")
    private String baseUrl;


    @Scheduled(fixedRate = 20000)
    public void sendTokenEmails() {
        System.out.println("sendTokenEmails");
        List<Token> tokenList = tokenService.getUnusedTokenList();
        if (!tokenList.isEmpty()) {
            tokenList.forEach(token -> {
                System.out.println(token.getUser().getEmail());
                System.out.println(emailSender);
                String link = activationBaseUrl + token.getToken();
                SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
                simpleMailMessage.setFrom(emailSender);
                simpleMailMessage.setTo(token.getUser().getEmail());
                simpleMailMessage.setSubject("ACTIVATION PROFILE");
                simpleMailMessage.setText("Salam Hormetli " + token.getUser().getName() + " " + token.getUser().getSurname() + " Zehmet olmasa profilinizi aktivlesdirmek ucun linke " +
                        " kecid edin : \n  " + link);

                emailService.sendEmail(simpleMailMessage);
                System.out.println("Email Sended !");
                tokenService.updateTokenStatus(token.getId());
            });

        }

    }

    @Scheduled(fixedRate = 40000)
    public void sendUpdatePassword() {
        System.out.println("Update Password Sended");

        tokenService.getUnusedTokenForUpdatePassword().forEach(token -> {

            String link = baseUrl + "updatePassword?token=" + token.getToken();
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setFrom(emailSender);
            mailMessage.setTo(token.getUser().getEmail());
            mailMessage.setSubject("UPDATE PASSWORD");
            mailMessage.setText("Dear, " + token.getUser().getName() + " " + token.getUser().getSurname() + ". \n For" +
                    " Update Your Password Please Click link below : \n "
                    + link);
            emailService.sendEmail(mailMessage);
            tokenService.updateStatusTokenForUpdatePass(token.getId());
        });


    }


}
