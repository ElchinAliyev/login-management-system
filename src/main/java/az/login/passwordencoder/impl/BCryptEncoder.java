package az.login.passwordencoder.impl;

import az.login.passwordencoder.PasswordEncoder;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Component;

@Component
public class BCryptEncoder implements PasswordEncoder {


    @Override
    public String encodePassword(String password) {
       return BCrypt.hashpw(password,BCrypt.gensalt());
    }
}
