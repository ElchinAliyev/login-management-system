package az.login.passwordencoder;

public interface PasswordEncoder {

    public String encodePassword(String password);

}
