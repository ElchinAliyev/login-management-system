package az.login.service;

import az.login.domain.User;

import java.util.Optional;

public interface UserService {
     User insertUser(User user);
     void updateUserStatus(long userId,long statusId);
     Optional<User> getUser(String email);
     void updateUserPass(long userId,String password);
}
