package az.login.service;

import org.springframework.mail.SimpleMailMessage;

public interface EmailService {

    public boolean emailExists(String email);
    public void sendEmail(SimpleMailMessage simpleMailMessage);


}
