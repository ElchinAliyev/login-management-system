package az.login.service;

import az.login.domain.Token;
import az.login.domain.User;
import java.util.List;
import java.util.Optional;

public interface TokenService {
     Token insertToken(User user);
     List<Token> getUnusedTokenList();
     void updateTokenStatus(Long tokenId);
     Optional<User> getUserByToken(String token);
     Token insertTokenForUpdatePassword(User user);
     List<Token> getUnusedTokenForUpdatePassword();
     void updateStatusTokenForUpdatePass(long tokenId);
     Optional<User> getUserByTokenForUpdatePass(String token);

}
