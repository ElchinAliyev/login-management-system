package az.login.service;

import az.login.domain.Image;
import az.login.domain.User;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ImageService {

    List<Image> insertImage(long id , MultipartFile[] images);
    int getUserImageCount(User user);
    List<Image> getImages(User user);
    Resource getImageById(int id);
}
