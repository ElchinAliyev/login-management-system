package az.login.service.impl;

import az.login.domain.Image;
import az.login.domain.User;
import az.login.repository.JpaImageRepository;
import az.login.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
public class ImageServiceImpl implements ImageService {

    @Autowired
    private JpaImageRepository imageRepository;

//    private static final String pathUrl = "C:\\Users\\nadir\\images";

        private static final String pathUrl = "/images";
    static {

        Path path = Paths.get(pathUrl);
        try {
            if (!Files.exists(path)) {
                Files.createDirectory(path);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Image> insertImage(long id, MultipartFile[] images) {
        List<Image> imageList = new ArrayList<>();
        User user = new User();
        user.setId(id);

        Path path = Paths.get(pathUrl + File.separator + id);
        if (!Files.exists(path)) {
            try {
                Files.createDirectory(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for (MultipartFile multipartFile : images) {
            File file = new File(path + File.separator + multipartFile.getOriginalFilename());
            System.out.println(file.getPath());
            try {
                Files.write(file.toPath(), multipartFile.getBytes());
                Image image = new Image();
                image.setUser(user);
                image.setImagePath(file.getPath());
                imageList.add(image);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return insertImages(imageList);

    }

    @Override
    public List<Image> getImages(User user) {
        return imageRepository.getAllByUser(user);
    }


    @Override
    public Resource getImageById(int id) {
        Image image = imageRepository.getOne(id);

        Resource resource = null;

        try {
            resource = new UrlResource("file:///" +image.getImagePath());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return resource;
    }

    @Override
    public int getUserImageCount(User user) {
        return imageRepository.getAllByUser(user).size();
    }

    public List<Image> insertImages(List<Image> imageList) {
        return imageRepository.saveAll(imageList);
    }
}
