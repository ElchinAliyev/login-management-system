package az.login.service.impl;

import az.login.domain.Token;
import az.login.domain.User;
import az.login.passwordencoder.PasswordEncoder;
import az.login.repository.UserRepository;
import az.login.service.TokenService;
import az.login.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private TokenService tokenService;

    @Transactional
    @Override
    public User insertUser(User user) {
        user.setPassword(passwordEncoder.encodePassword(user.getPassword()));

        System.out.println("Hashed pass = " +user.getPassword());
         user = userRepository.insertUser(user);
        Token token = tokenService.insertToken(user);
        return user;

    }


    @Transactional
    @Override
    public void updateUserStatus(long userId, long statusId) {
        userRepository.updateUserStatus(userId,statusId);
    }

    @Override
    public Optional<User> getUser(String email) {
        return userRepository.getUser(email);
    }

    @Override
    public void updateUserPass(long userId, String password) {
            String hashedPassword = passwordEncoder.encodePassword(password);
           userRepository.updateUserPass(userId,hashedPassword);
    }


}
