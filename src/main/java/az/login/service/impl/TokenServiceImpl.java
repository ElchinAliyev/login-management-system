package az.login.service.impl;

import az.login.domain.Token;
import az.login.domain.User;
import az.login.repository.TokenRepository;
import az.login.service.TokenService;
import az.login.util.CommonUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class TokenServiceImpl implements TokenService {
    @Autowired
    private TokenRepository tokenRepository;

    @Override
    public Token insertToken(User user) {
        Token token = new Token();
        token.setUser(user);
        token.setToken(CommonUtility.generateToken());
        return tokenRepository.insertToken(token);

    }

    @Override
    public List<Token> getUnusedTokenList() {
        return tokenRepository.getUnusedTokens();
    }

    @Transactional
    @Override
    public void updateTokenStatus(Long tokenId) {
        tokenRepository.updateTokenStatus(tokenId);
    }

    @Override
    public Optional<User> getUserByToken(String token) {


        return tokenRepository.getUserByToken(token);
    }

    @Override
    public Token insertTokenForUpdatePassword(User user) {
        Token token = new Token();
        token.setToken(CommonUtility.generateToken());
        token.setUser(user);
       return tokenRepository.insertTokenForUpdatePassword(token);
    }

    @Override
    public List<Token> getUnusedTokenForUpdatePassword() {
        return tokenRepository.getUnusedTokenForUpdatePassword();
    }

    @Override
    public void updateStatusTokenForUpdatePass(long tokenId) {
            tokenRepository.updateStatusTokenForUpdatePass(tokenId);
    }

    @Override
    public Optional<User> getUserByTokenForUpdatePass(String token) {
        return tokenRepository.getUserByTokenForUpdatePass(token);
    }
}
