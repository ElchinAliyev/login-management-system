package az.login.service.impl;

import az.login.repository.EmailRepository;
import az.login.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;


@Service
public class EmailServiceImpl implements EmailService {


    @Autowired
private EmailRepository emailRepository;

@Autowired
private JavaMailSender mailSender;

    @Override
    public boolean emailExists(String email) {
        return emailRepository.emailExists(email);
    }


    @Async
    @Override
    public void sendEmail(SimpleMailMessage simpleMailMessage) {
     mailSender.send(simpleMailMessage);
    }
}
