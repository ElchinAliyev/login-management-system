package az.login.security;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SpringSecurity extends WebSecurityConfigurerAdapter {



    @Autowired
    private SuccessEventHandler successEventHandler;

    @Autowired
    private FailureEventHandler failureEventHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .formLogin()
                .loginPage("/")
                .usernameParameter("email")
                .permitAll()
                .successHandler(successEventHandler)
                .failureHandler(failureEventHandler)
                .and()
                .authorizeRequests()
                .antMatchers("/user/**")
                .hasAnyRole("USER")
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID");
    }

    @Autowired
    private UserSecurityService securityService ;
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception  {
        System.out.println("Config isledi !");
        auth.authenticationProvider(getAuthenticationProvider());
    }
    
    
    @Bean
    public DaoAuthenticationProvider getAuthenticationProvider(){
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(securityService);
        daoAuthenticationProvider.setPasswordEncoder(getBCryptpasswordEncoder());
        return daoAuthenticationProvider;
    }
    
    @Bean
    public PasswordEncoder getBCryptpasswordEncoder(){
        return  new BCryptPasswordEncoder();
    }


}
