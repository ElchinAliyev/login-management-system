package az.login.security;

import az.login.domain.User;
import az.login.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserSecurityService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<User> optionalUser = userService.getUser(s);
        UserPrincipal userPrincipal = null;
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            userPrincipal = new UserPrincipal();
            userPrincipal.setUser(user);
            System.out.println(userPrincipal);
            return userPrincipal;

        }
        else {

            throw new UsernameNotFoundException("User tapilmadi " + s);
        }

    }
}
