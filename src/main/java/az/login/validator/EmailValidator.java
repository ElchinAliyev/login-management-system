package az.login.validator;

import az.login.domain.Email;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Component
public class EmailValidator implements Validator {



    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(Email.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
 Email email = (Email) o;

        System.out.println("Mail Validation isledi");
 String regex = "^(.+)@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email.getEmail());
        if (!matcher.matches()){
            errors.rejectValue("email","email.error");
        }
    }
}
